#include <SoftwareSerial.h> //flame
#include <LiquidCrystal.h>
LiquidCrystal LCD(11,10,9,2,3,4,5);  //Create Liquid Crystal Object called LCD
#define motorPin 6
#define trigPin 13
#define echoPin 12
long duration, distance;
int sensorPin = A0; // select the input pin for the LDR
int sensorValue = 0; // variable to store the value coming from the sensor

void setup() {
  LCD.begin(16,2); //Tell Arduino to start your 16 column 2 row LCD
  
  Serial.begin (9600);
  pinMode (motorPin, OUTPUT);
  pinMode (trigPin, OUTPUT);
  pinMode (echoPin, INPUT);
}

void loop() {
  digitalWrite(trigPin, LOW);        
  delayMicroseconds(2);              
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);           
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;

  LCD.setCursor(0,0); 
  LCD.print("                ");
  LCD.setCursor(0,0);  //Set LCD cursor to upper left corner, column 0, row 0
  LCD.print("Target Distance:");  //Print Message on First Row
  LCD.setCursor(0,1);  //Set cursor to first column of second row
  LCD.print("                "); //Print blanks to clear the row
  LCD.setCursor(0,1);   //Set Cursor again to first column of second row
  LCD.print(distance); //Print measured distance
  LCD.print(" cm");  //Print your units.

  if (distance >= 500 || distance <= 0)
  {
    Serial.println("Out of range");
  }
  else 
  {
    Serial.print(distance);
    Serial.println(" cm");
  if (distance <=50)
  {
    analogWrite (motorPin,245);
  }
  if (distance > 50 && distance <=100)
   {
    analogWrite (motorPin,230);
   }
  if (distance > 100)
    {
      analogWrite (motorPin,220); 
    }
  }

  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  if (sensorValue < 110)// val mai mare pt distanta mai mare la citire
    {
      Serial.println("Fire Detected");
      Serial.print (sensorValue);
      LCD.setCursor(0,0); 
      LCD.print("                ");
      LCD.setCursor(0,1);   
      LCD.print("                "); 
      LCD.setCursor(0,0); 
      LCD.print("Fire Detected");
      analogWrite (motorPin, 245);
      delay(1000);
    }

    if (sensorValue > 170)// val mai mare pt distanta mai mare la citire
    {
      Serial.println("Sound Detected");
      Serial.print (sensorValue);
      LCD.setCursor(0,0); 
      LCD.print("                ");
      LCD.setCursor(0,1);   
      LCD.print("                "); 
      LCD.setCursor(0,0); 
      LCD.print("Sound Detected");
      analogWrite (motorPin, 245);
      delay(1000);
    }
  delay(sensorValue);
  
 delay(250); //pause to let things settle 
}
